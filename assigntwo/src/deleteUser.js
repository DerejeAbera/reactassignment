import React, {useState} from "react";
import {IconButton} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {RemoveUser} from "./service/userService";
import DisplayUsers from "./displayUsers";
import {Route} from "react-router-dom";



export default function DeleteUser(props){
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleDeleteUser =() =>{
        let id=props.user.id;
        setOpen(false);
        console.log(props.user.username);
        console.log(id);
        RemoveUser(id);

    };
    return(
        <>
            <IconButton  edge="end" arial-label="deletedit" onClick={handleClickOpen}>
                <DeleteIcon/>
            </IconButton>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Are you sure to delete?"}
                </DialogTitle>

                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Disagree
                    </Button>
                    <Button onClick={handleDeleteUser} color="primary" autoFocus >
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}