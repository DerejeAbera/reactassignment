import React from 'react';
import logo from "./logo.jpg";
import Button from "@material-ui/core/Button";
import {Link } from "react-router-dom";
import {Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";


const paperStyle={padding:10,height:'70vh',width: 900,margin: "50px auto"}

export default function  MainBody () {

    return (
        <Grid style={{padding:20,backgroundColor:'#b4d3e1',height:'100vh',width: 1200,margin: "70px auto" }}>
            <Paper elevation={10} style={paperStyle}>
                <div>
                    <img src={logo} alt="logo" width="800" height="200" style={{ display: 'block',marginLeft: 'auto',
                        marginRight: 'auto',width: '75%', marginTop: '70px'}}/>

                    <Button  variant="contained" color="primary" style={{float: 'left', marginLeft: '120px',}}>
                        <Link to="/createUser"  style={{color: 'hotpink' }}>
                            create a User
                        </Link>
                    </Button>
                    <Button variant="contained" color="primary" style={{float: 'center', marginLeft: '158px'}}>
                        <Link to="/login"  style={{color: 'hotpink' }}>
                            Log in
                        </Link>
                    </Button>
                    <Button  variant="contained" color="primary" style={{float: 'right', marginRight: '180px'}} >
                        <Link to="/displayUsers"  style={{color: 'hotpink' }}>
                            USERS
                        </Link>
                    </Button>

                </div>

            </Paper>
        </Grid>
    );
}