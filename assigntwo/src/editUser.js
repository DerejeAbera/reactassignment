import React, {useState} from "react";
import {IconButton} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import {UpdateUser} from "./service/userService";


export default function EditUser(props){
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(props.value);
    const handleClickOpen =() =>{
        setOpen(true);
    };

    const handleClose =() =>{
        setOpen(false);
    };

    const handleChangeUsername =() =>{

        setOpen(false);
        console.log(value);
        console.log(props.user.id);
        UpdateUser(props.user.id,value);
    };

    return(
        <>
            <IconButton  edge="end" arial-label="deletedit" onClick={handleClickOpen}>
                <EditIcon/>
            </IconButton>
            <Dialog open={open} onClose={handleClose} arial-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title"> Edit user name </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        To change user name
                    </DialogContentText>
                    <TextField

                        autoFocus
                        margin="dense"
                        id="name"
                        label="username"
                        type="email"
                        value={value}
                        onChange={event => setValue(event.target.value)}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>

                    <Button onClick={handleChangeUsername} color="primary">
                        Change username
                    </Button>

                </DialogActions>
            </Dialog>

        </>
    );
}






