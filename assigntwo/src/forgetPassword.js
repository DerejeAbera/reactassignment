import React, { Component } from 'react';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import {Avatar, Grid, Paper} from "@material-ui/core";
import VpnKeyOutlinedIcon from '@material-ui/icons/VpnKeyOutlined';




const paperStyle={padding:100,height:'90vh',width: 500,margin: "70px auto",backgroundColor:'#ffffff'}
const avatarStyle={backgroundColor:'#1bb67e'}
const btnstyle={margin:'15px 0'}

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));

export default class ForgetPassword extends Component {
    state = {
        username: "",
        newPassword: "",
        confirmPassword:"",
    }

    onUserNameChange = (e) => {
        e.preventDefault();
        this.setState({ username: e.target.value });
    }
    onNewPasswordChange = (e) => {
        e.preventDefault();
        this.setState({ newPassword: e.target.value });
    }
    onConfirmPasswordChange = (e) => {
        e.preventDefault();
        this.setState({ confirmPassword: e.target.value });
    }

    onSubmit = (e) => {
        e.preventDefault();
        e.preventDefault();
        let aUser={
            username: this.state.username,
            newPassword: this.state.newPassword,
            confirmPassword: this.state.confirmPassword,

        }
        //send
        console.log(aUser);
        this.state.username='';
        this.state.newPassword='' ;
        this.state.confirmPassword='' ;
    }
    render(){

        return (

            <Grid style={{padding:20,backgroundColor:'#212121',height:'150vh',width: 900,margin: "70px auto" }}>
                <Paper elevation={10} style={paperStyle}>

                    <Grid align='center'>
                        <Avatar style={avatarStyle}><VpnKeyOutlinedIcon/> </Avatar>
                        <h2> Forget/change Your Password </h2> <br/>
                    </Grid>

                    <TextField id="username"label="username" value={this.state.username}
                               onChange={this.onUserNameChange} fullWidth required/> <br/> <br/> <br/>

                    <TextField
                        hintText="At least 8 characters"
                        floatingLabelText="Enter your password"
                        errorText="Your password is too short"
                        label="newPassword"
                        id="newPassword"
                        value={this.state.newPassword}
                        onChange={this.onNewPasswordChange}
                        fullWidth required/><br/> <br/> <br/>

                    <TextField id="confirmPassword"label="confirmPassword" value={this.state.confirmPassword}
                               onChange={this.onConfirmPasswordChange} fullWidth required/> <br/> <br/> <br/>

                    <Button type='submit' color='primary' variant='contained' style={btnstyle}
                            disabled={this.state.username.length === 0 ||
                            this.state.password.length === 0}
                            onClick={this.onSubmit} fullWidth> Creat a new password </Button>


                </Paper>
            </Grid>
        );
    }

}
