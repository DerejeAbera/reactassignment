import React, { Component, useState} from 'react';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import PasswordField from 'material-ui-password-field'
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import {Paper,Avatar, Grid} from "@material-ui/core";
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

import {Link } from "react-router-dom";


const user={
    aUsername:"",
    aPassword:"",
}
const paperStyle={padding:50,height:'70vh',width: 350,margin: "70px auto",backgroundColor:'#ffffff'}
const avatarStyle={backgroundColor:'#1bb67e'}
const btnStyle={margin:'8px 0'}



class Login extends Component {

    state = {
        username: "",
        password: "",
    }

    onUserNameChange = (e) => {
        e.preventDefault();
        this.setState({ username: e.target.value });
    }
    onPasswordChange = (e) => {
        e.preventDefault();
        this.setState({ password: e.target.value });
    }

    onSubmit = (e) => {
        e.preventDefault();
        let userLogin={
            username: this.state.username,
            password: this.state.password,
        }
        console.log(userLogin);
        //send, call the method that you creat at the service , post

        this.state.username='';
        this.state.password='' ;
    }
    render(){

        return (

            <Grid style={{padding:20,backgroundColor:'#49acfb',height:'120vh',width: 800,margin: "70px auto" }}>
                <Paper elevation={10} style={paperStyle}>

                    <Grid align='center'>
                        <Avatar style={avatarStyle}><LockOpenOutlinedIcon/> </Avatar>
                        <h2> Log in</h2>
                    </Grid>

                    <TextField id="username"label="username" placeholder="Enter username" value={this.state.username}
                               onChange={this.onUserNameChange} fullWidth required/>

                    <TextField id="password" label="password" placeholder="Enter password" type='password'
                               value={this.state.password}
                               onChange={this.onPasswordChange} fullWidth required />

                    <FormControlLabel
                        control={
                            <Checkbox

                                name="checkedB"
                                color="primary"
                            />
                        }
                        label="Remember me"
                    />

                    <Button type='submit' color='primary' variant='contained' style={btnStyle}
                            disabled={this.state.username.length === 0 ||
                            this.state.password.length === 0}
                            onClick={this.onSubmit} fullWidth> Login </Button>

                    <Typography >
                        <Link to="/forgetPassword" style={{color: '#4caf50' }}>
                            Forgot password?
                        </Link>

                    </Typography>

                    <Typography> Do you have an account?
                        <Link to="/createUser" style={{color: 'hotpink' }}>
                            creat an account
                        </Link>

                    </Typography>

                </Paper>

            </Grid>


        );
    }

}

export default Login;

