import Header from "../header";
import MainBody from "../mainBody";
import Footer from "../footer";
import React from "react";

const Home = () => {
    return (
        <>
            <Header/>
    <MainBody/>
    <Footer designedBy="@dereje.com"/>
        </>
);
}
export default Home;