import React from 'react'
import {    BrowserRouter as Router,
    Route, Switch
} from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/about";
import CreateUser from "./createUser";
import DisplayUsers from "./displayUsers";
import Login from "./login";
import ForgetPassword from "./forgetPassword";


export default function Routes() {
    return(
        <Router>
            <Switch>
                <Route  path='/about' component={About}/>
                <Route path="/login" component={Login}/>
                <Route  path='/createUser' component={CreateUser}/>
                <Route path="/forgetPassword" component={ForgetPassword}/>
                <Route exact path='/displayUsers' component={DisplayUsers}/>

                <Route path="/" component={Home}/>



            </Switch>
        </Router>
    );
}