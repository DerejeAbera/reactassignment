
const url = "https://usersjava20.sensera.se/users";

export const saveUser=(user) =>{
    // users only for users domain for domain
    fetch(url, {
        method: "POST",
        body: JSON.stringify(user),
        headers: { "Content-Type": "application/json" }
    })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => {
            console.error("Error:", error);
        });

};

export function getMe(){
    return fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" }
    }).then(response => response.json());

};
export const getUsers=()=>
{
    fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" }
    })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => {
            console.error("Error:", error);
        });
};
export const findUser=(username)=>{
    return fetch("https://usersjava20.sensera.se/users/find?username="+username, {
        method: "GET",
        headers: { "Content-Type": "application/json" }
    })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => {
            console.error("Error:", error)
        });
}
export const getUser=(id)=>
{
    fetch("https://usersjava20.sensera.se/users"+id, {
        method: "GET",
        headers: { "Content-Type": "application/json" }
    })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => {
            console.error("Error:", error);
        });

};

export const RemoveUser=(id) =>{
    // users only for users domain for domain
    fetch("https://usersjava20.sensera.se/users/"+id, {
        method: "DELETE",
    }).then(response => response.json())
        .then(data => console.log(data))
        .catch(error => {
            console.error("Error:", error);
        });

};

export const UpdateUser=(id,username) =>{
    // users only for users domain for domain
    fetch("https://usersjava20.sensera.se/users/"+id, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
            username:username,
            password:'Jafer123'
        }),
    })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => {
            console.error("Error:", error);
        });
};


