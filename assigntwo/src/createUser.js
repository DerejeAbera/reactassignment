import React, { Component } from 'react';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import PasswordField from "material-ui-password-field";
import {Avatar, Grid, Paper} from "@material-ui/core";
import VpnKeyOutlinedIcon from '@material-ui/icons/VpnKeyOutlined';
import  {saveUser} from './service/userService';
import {Link} from "react-router-dom";


const paperStyle={padding:100,height:'90vh',width: 500,margin: "70px auto",backgroundColor:'#ffffff'}
const avatarStyle={backgroundColor:'#1bb67e'}
const btnStyle={margin:'8px 0'}


const url = "https://usersjava20.sensera.se/users";
const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));

class CreateUser extends Component {
    state = {
        username: "",
        password: "",
        description:"",
        open:true,
    }

    onUserNameChange = (e) => {
        e.preventDefault();
        this.setState({ username: e.target.value });
    }
    onPasswordChange = (e) => {
        e.preventDefault();
        this.setState({ password: e.target.value });
    }
    onDescriptionChange = (e) => {
        e.preventDefault();
        this.setState({ description: e.target.value });
    }

    onSubmit = (e) => {
        this.state.open=false;
        e.preventDefault();

        let aUser={
            username: this.state.username,
            password: this.state.password,
            description: this.state.description
        }
        saveUser(aUser);

        console.log(aUser);

        // we should put the field empty

        this.state.username='';
        this.state.password='';
        this.state.description='';

    }

    render()
    {
        return (
            <Grid style={{padding:20,backgroundColor:'#4a3933',height:'150vh',width: 900,margin: "70px auto" }}>
                <Paper elevation={10} style={paperStyle} >

                    <Grid align='center'>
                        <Avatar style={avatarStyle}><VpnKeyOutlinedIcon/> </Avatar>
                        <h2> Creat an account </h2> <br/>
                    </Grid>

                    <TextField id="username"label="username" value={this.state.username}
                               onChange={this.onUserNameChange} fullWidth required/> <br/> <br/> <br/>

                    <TextField
                        hintText="At least 8 characters"
                        floatingLabelText="Enter your password"
                        errorText="Your password is too short"
                        label="password"
                        id="password"
                        type="password"
                        value={this.state.password}
                        onChange={this.onPasswordChange}
                        fullWidth required/><br/> <br/> <br/>

                    <TextField id="description"label="description" value={this.state.description}
                               onChange={this.onDescriptionChange} fullWidth required/> <br/> <br/> <br/>




                    <Button type='submit' color='primary' variant='contained' style={btnStyle}
                            disabled={this.state.username.length === 0 ||
                            this.state.password.length === 0}
                            onClick={this.onSubmit} fullWidth  > Creat a User
                        <Link to="/">  </Link>
                    </Button>

                </Paper>
            </Grid>
        );
    }

}

export default  CreateUser ;