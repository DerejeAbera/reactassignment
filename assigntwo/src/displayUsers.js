
import {getMe, UpdateUser,RemoveUser} from "./service/userService";
import React, {useEffect, useState} from "react";
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Avatar} from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import ImageIcon from "@material-ui/icons/Image";

import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import EditUser from "./editUser";
import DeleteUser from "./deleteUser";
import Container from "@material-ui/core/Container";



export default function  DisplayUsers  ()  {
    const [users, setUsers] = useState([]);
    //const [oldUserName, setOldUserName]=useState();
    const  [list,setList]  = React.useState(users)  ;

    const onDelete=(id, username)=>{
        console.log(id );
        console.log(username );
        //RemoveUser(id);
        //onDeleteValue={deletedUsername => onDelete(user.id, deletedUsername.target.value)}
    }
    const  onChangeUserName = (id, newUsername) =>{
/*
        setList(list=>   {
            return list.map(user => {
                if(user.id === id){
                    user.name = newUsername;
                }
                console.log(user );
                console.log(newUsername);
                console.log(user.id);
                console.log(id);
                //return user;
            })
        })*/
        //onChangeValue={changeName => onChangeUserName(user.id, changeName)}
        console.log(id );
        console.log(newUsername);

        UpdateUser(id,newUsername);
    };

    useEffect(() => {
        getMe().then(data => setUsers(data));
    },[]);

    return (
        <Container maxWidth="sm">

            <List>
                <h2> Users</h2>
                {
                    users.map(user => (

                        <ListItem key={user.id}>

                            <ListItemAvatar>
                                <Avatar>

                                    <ImageIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText
                                primary={user.username}
                                secondary={user.description}

                            />

                            <ListItemSecondaryAction>

                                < EditUser value={user.username}

                                           user={user}/>
                                <DeleteUser value={user.username}
                                            user={user}
                                            v-show={<DisplayUsers/>}
                                />
                            </ListItemSecondaryAction>


                        </ListItem>
                    ))
                }
            </List>
        </Container>
    );
}