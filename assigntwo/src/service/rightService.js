const url = "https://usersjava20.sensera.se/right";

export const saveRight=(right) =>{
    // users only for users domain for domain
    fetch(url, {
        method: "POST",
        body: JSON.stringify(right),
        headers: { "Content-Type": "application/json" }
    })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => {
            console.error("Error:", error);
        });

};

export function getMe(){
    return fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" }
    }).then(response => response.json());

};
export const getRight=()=>
{
    fetch(url, {
        method: "GET",
        headers: { "Content-Type": "application/json" }
    })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => {
            console.error("Error:", error);
        });
};